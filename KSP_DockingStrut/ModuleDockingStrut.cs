﻿/* Name: Docking Strut
   Version: 1.0.1.0
   Author: JDP

   This code is free to use and modify as long as the original author is credited.
   
   Changelog:
   1.0.0.1
   - Implemented more realistic strut physics. now each end of the strut is linked to the strut anchors.
   - Fixed an exploit where relinking didn't check for valid links.
   - Added a whole bunch of extra actionGroups; unling, relink & toggle link.
   - Added a suite of cfg-configurable variables to make it a lot easier for partmodelers to create their own docking struts.

   1.0.1.0
   - Updated plugin and part to KSP 21.1 (finally).
   - Reworked joint linkage to get rid of phantom forces.
   - Added new ID system.
   - Reworked the way the visual strut is rescaled.
*/

using System;
using System.Linq;
using UnityEngine;

namespace DockingStrut
{
    public class ModuleDockingStrut : PartModule
    {
        private bool _clas;
        [KSPField(isPersistant = true, guiActive = true)] 
        public string IDs = Guid.Empty.ToString();
        [KSPField] 
        public float MaxDistance = 10;
        private float _strutX, _strutY;
        public ModuleDockingStrut TargetDS;
        [KSPField(isPersistant = true, guiActive = true)] 
        public string TargetIDs = Guid.Empty.ToString();
        [KSPField(isPersistant = true)] 
        public bool Targeted = false;
        public ModuleDockingStrut TargeterDS;
        private bool _checkForReDocking;
        public ConfigurableJoint Joint;
        private bool _jointCreated;
        private Transform _mRco, _mSt, _mStrut;
        public DSMode Mode;
        [KSPField] 
        public string RayCastOriginName = "strut";
        private bool _started;
        [KSPField] 
        public string StrutName = "strut";
        [KSPField] 
        public string StrutTargetName = "strut";
        [KSPField] 
        private int _ticksToCheckForLinkAtStart = 100;

        public Guid ID
        {
            get { return new Guid(this.IDs); }
            set { this.IDs = value.ToString(); }
        }

        public Guid TargetID
        {
            get { return new Guid(this.TargetIDs); }
            set { this.TargetIDs = value.ToString(); }
        }

        public Vector3 RayCastOrigin
        {
            get { return this._mRco.position; }
        }

        public Vector3 StrutTarget
        {
            get { return this._mSt.position; }
        }

        [KSPEvent(name = "Abort", active = false, guiName = "Abort link", guiActiveUnfocused = true, unfocusedRange = 50)]
        public void Abort()
        {
            this.Targeted = false;
            this.Mode = DSMode.Unlinked;
            foreach (var moduleDockingStrut in (from p in this.vessel.parts
                                                where p.Modules.Contains("ModuleDockingStrut")
                                                select p.Modules["ModuleDockingStrut"]).OfType<ModuleDockingStrut>())
            {
                moduleDockingStrut.RevertGui();
            }
            this.Events["Abort"].active = this.Events["Abort"].guiActive = false;
        }

        [KSPEvent(name = "ErrorMessage", active = false, guiName = "", guiActiveUnfocused = true, unfocusedRange = 50)]
        public void ErrorMessage()
        {
        }

        [KSPEvent(name = "Link", active = false, guiName = "Link", guiActiveUnfocused = true, unfocusedRange = 50)]
        public void Link()
        {
            foreach (var p in this.vessel.parts.Where(p => p != this.part && p.Modules.Contains("ModuleDockingStrut")))
            {
                DSUtil.SetPossibleTarget(this, (p.Modules["ModuleDockingStrut"] as ModuleDockingStrut));
            }
            this.Mode = DSMode.Targeting;
            this.Events["Link"].active = this.Events["Link"].guiActive = false;
        }

        public override void OnStart(StartState state)
        {
            this._mStrut = this.part.FindModelTransform(this.StrutName);
            this._strutX = this._mStrut.localScale.x;
            this._strutY = this._mStrut.localScale.y;
            this._mStrut.localScale = Vector3.zero;
            if (state == StartState.Editor)
            {
                return;
            }
            this._mRco = this.part.FindModelTransform(this.RayCastOriginName);
            this._mSt = this.part.FindModelTransform(this.StrutTargetName);
            if (this.ID == Guid.Empty)
            {
                this.ID = Guid.NewGuid();
            }
            if (this.Targeted)
            {
                this.SetTargetDSAtLoad();
            }
            else
            {
                this.Mode = DSMode.Unlinked;
            }
            this._started = true;
        }

        public override void OnUpdate()
        {
            if (!this._started)
            {
                return;
            }
            this.UpdateGui();
            this.UpdateLink();
            if (!this._clas)
            {
                return;
            }
            if (DSUtil.CheckPossibleTarget(this, this.TargetDS))
            {
                this._clas = false;
                this.SetTarget(this.TargetDS);
            }
            else if (this._ticksToCheckForLinkAtStart-- < 0)
            {
                this._clas = false;
            }
        }

        [KSPAction("RelinkActon", KSPActionGroup.None, guiName = "Relink")]
        public void RelinkActon(KSPActionParam param)
        {
            if (this.Mode != DSMode.Linked && this.TargetDS != null && this.TargetDS.vessel == this.vessel)
            {
                this.SetTarget(this.TargetDS);
            }
        }

        [KSPEvent(name = "SetAsTarget", active = false, guiName = "Set as target", guiActiveUnfocused = true, unfocusedRange = 50)]
        public void SetAsTarget()
        {
            this.TargeterDS.SetTarget(this);
            foreach (var moduleDockingStrut in (from p in this.vessel.parts
                                                where p.Modules.Contains("ModuleDockingStrut")
                                                select p.Modules["ModuleDockingStrut"]).OfType<ModuleDockingStrut>())
            {
                moduleDockingStrut.RevertGui();
            }
            this.Events["SetAsTarget"].active = this.Events["SetAsTarget"].guiActive = false;
        }

        public void SetErrorMessage(String s)
        {
            this.Mode = DSMode.Invalid;
            foreach (var e in this.Events)
            {
                if (e.name.Equals("ErrorMessage"))
                {
                    e.active = e.guiActive = true;
                    e.guiName = s;
                }
                else
                {
                    e.active = e.guiActive = false;
                }
            }
        }

        private void SetStrutEnd(Vector3 position)
        {
            this._mStrut.LookAt(position);
            this._mStrut.localScale = new Vector3(this._strutX, this._strutY, 1);
            this._mStrut.localScale = new Vector3(this._strutX, this._strutY, Vector3.Distance(Vector3.zero, this._mStrut.InverseTransformPoint(position)));
        }

        public void SetTarget(ModuleDockingStrut posTarget)
        {
            if (!DSUtil.CheckPossibleTarget(this, posTarget))
            {
                this.Mode = DSMode.Unlinked;
                this.Targeted = false;
                return;
            }
            foreach (var e in this.Events)
            {
                e.active = e.guiActive = false;
            }
            this.TargetDS = posTarget;
            this.TargetID = this.TargetDS.ID;
            this.Targeted = true;
            this.Mode = DSMode.Linked;
            this.SetStrutEnd(this.TargetDS.StrutTarget);
        }

        public void SetTargetDSAtLoad()
        {
            if (this.vessel.GetDS(this.TargetID, out this.TargetDS))
            {
                this._clas = true;
            }
            else
            {
                foreach (var e in this.Events)
                {
                    e.active = e.guiActive = false;
                }
                this.Mode = DSMode.Unlinked;
                this.Targeted = false;
            }
        }

        [KSPAction("ToggleActon", KSPActionGroup.None, guiName = "Toggle link")]
        public void ToggleActon(KSPActionParam param)
        {
            if (this.Mode == DSMode.Linked)
            {
                this.Unlink();
            }
            else if (this.TargetDS != null && this.TargetDS.vessel == this.vessel)
            {
                this.SetTarget(this.TargetDS);
            }
        }

        [KSPEvent(name = "Unlink", active = false, guiName = "Unlink", guiActiveUnfocused = true, unfocusedRange = 50)]
        public void Unlink()
        {
            this.Targeted = false;
            this.Mode = DSMode.Unlinked;

            this.Events["Unlink"].active = this.Events["Unlink"].guiActive = false;
        }

        [KSPAction("UnlinkActon", KSPActionGroup.None, guiName = "Unlink")]
        public void UnlinkActon(KSPActionParam param)
        {
            if (this.Mode == DSMode.Linked)
            {
                this.Unlink();
            }
        }

        private void UpdateGui()
        {
            switch (this.Mode)
            {
                case DSMode.Linked:
                {
                    this.Events["Unlink"].active = this.Events["Unlink"].guiActive = true;
                }
                    break;
                case DSMode.Target:
                {
                    this.Events["SetAsTarget"].active = this.Events["SetAsTarget"].guiActive = true;
                }
                    break;
                case DSMode.Targeting:
                {
                    this.Events["Abort"].active = this.Events["Abort"].guiActive = true;
                }
                    break;
                case DSMode.Unlinked:
                {
                    this.Events["Link"].active = this.Events["Link"].guiActive = true;
                }
                    break;
            }
        }

        public void RevertGui()
        {
            this.Mode = this.Targeted ? DSMode.Linked : DSMode.Unlinked;
            foreach (var e in this.Events)
            {
                e.active = e.guiActive = false;
            }
        }

        private void UpdateLink()
        {
            try
            {
                if (this.Targeted && this.TargetDS != null)
                {
                    if (this.TargetDS.vessel != this.vessel)
                    {
                        this.Unlink();
                        this._checkForReDocking = true;
                    }
                    else
                    {
                        this.TargetID = this.TargetDS.ID;
                    }
                }
                if (this._checkForReDocking)
                {
                    if (this.Targeted || this.TargetDS == null)
                    {
                        this._checkForReDocking = false;
                    }
                    else if (this.TargetDS.vessel == this.vessel)
                    {
                        this.SetTarget(this.TargetDS);
                        this._checkForReDocking = false;
                    }
                }
                if (this._jointCreated == this.Targeted || this.part.rigidbody.isKinematic || this.TargetDS == null || this.TargetDS.part.rigidbody.isKinematic)
                {
                    return;
                }
                if (this.Targeted)
                {
                    this.Joint = this.part.rigidbody.gameObject.AddComponent<ConfigurableJoint>();
                    this.Joint.connectedBody = this.TargetDS.part.rigidbody;
                    this.Joint.breakForce = this.Joint.breakTorque = float.PositiveInfinity;
                    this.Joint.xMotion = ConfigurableJointMotion.Locked;
                    this.Joint.yMotion = ConfigurableJointMotion.Locked;
                    this.Joint.zMotion = ConfigurableJointMotion.Locked;
                    this.Joint.angularXMotion = ConfigurableJointMotion.Locked;
                    this.Joint.angularYMotion = ConfigurableJointMotion.Locked;
                    this.Joint.angularZMotion = ConfigurableJointMotion.Locked;
                }
                else
                {
                    Destroy(this.Joint);
                    this.Joint = null;
                    this._mStrut.localScale = Vector3.zero;
                }
                this._jointCreated = this.Targeted;
            }
            catch
            {
            }
        }
    }
}