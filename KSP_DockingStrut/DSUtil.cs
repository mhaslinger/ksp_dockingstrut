﻿using System;
using System.Linq;
using UnityEngine;

namespace DockingStrut
{
    public enum DSMode
    {
        Unlinked,
        Linked,
        Targeting,
        Target,
        Invalid
    }

    public static class DSUtil
    {
        public static bool GetDS(this Vessel v, Guid targetID, out ModuleDockingStrut target)
        {
            foreach (var p in from p in v.Parts
                              let moduleDockingStrut = p.Modules["ModuleDockingStrut"] as ModuleDockingStrut
                              where moduleDockingStrut != null && (p.Modules.Contains("ModuleDockingStrut") && moduleDockingStrut.ID == targetID)
                              select p)
            {
                target = (p.Modules["ModuleDockingStrut"] as ModuleDockingStrut);
                return true;
            }
            target = null;
            return false;
        }

        public static Part PartFromHit(RaycastHit hit)
        {
            var go = hit.collider.gameObject;
            Part p = null;
            while (p == null)
            {
                p = Part.FromGO(go);

                if (go.transform.parent != null && go.transform.parent.gameObject != null)
                {
                    go = go.transform.parent.gameObject;
                }
                else
                {
                    break;
                }
            }
            return p;
        }

        public static bool CheckPossibleTarget(ModuleDockingStrut origin, ModuleDockingStrut target)
        {
            try
            {
                var distance = Vector3.Distance(target.part.transform.position, origin.part.transform.position);
                if (distance > origin.MaxDistance)
                {
                    return false;
                }
                RaycastHit info;
                var start = origin.RayCastOrigin;
                var dir = (target.StrutTarget - start).normalized;
                var hit = Physics.Raycast(new Ray(start, dir), out info, origin.MaxDistance + 1);
                var tmpp = PartFromHit(info);
                if (hit && tmpp == target.part)
                {
                    hit = false;
                }
                return !hit;
            }
            catch
            {
                return false;
            }
        }

        public static void SetPossibleTarget(ModuleDockingStrut origin, ModuleDockingStrut target)
        {
            var distance = Vector3.Distance(target.part.transform.position, origin.part.transform.position);
            if (distance > origin.MaxDistance)
            {
                target.SetErrorMessage("Out of range by " + Math.Round(distance - origin.MaxDistance, 2) + "m");
                return;
            }
            RaycastHit info;
            var start = origin.RayCastOrigin;
            var dir = (target.StrutTarget - start).normalized;
            var hit = Physics.Raycast(new Ray(start, dir), out info, origin.MaxDistance + 1);
            var tmpp = PartFromHit(info);
            if (hit && tmpp == target.part)
            {
                hit = false;
            }
            if (hit)
            {
                target.SetErrorMessage("Obstructed by " + tmpp.name);
                return;
            }
            target.Mode = DSMode.Target;
            target.TargeterDS = origin;
            foreach (var e in target.Events)
            {
                e.active = e.guiActive = false;
            }
        }
    }
}